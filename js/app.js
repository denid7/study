const regEmail = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;

const form = document.querySelector('form'); 
form.addEventListener('submit', (e)=>{
    e.preventDefault();
    let name = form.querySelector('input[data-name]');
    let email = form.querySelector('input[data-email]');
    let pass = form.querySelector('input[data-pass]');
    let confirm = form.querySelector('input[data-confirm]');
    validateName(name); 
    validateEmail(email);
    checkPass(pass);
    confirmPass(confirm, pass);
    if(validateName(name) == true && validateEmail(email) == true && confirmPass(confirm, pass)){
        alert('Данные успешно записаны');
        localStorage.setItem('name', name.value);
        localStorage.setItem('email',email.value);
        localStorage.setItem('pass', pass.value);
        for(let key,i=0;i<localStorage.length;i++) key=localStorage.key(i),console.log(key,':', localStorage.getItem(key));
    }else{
        localStorage.clear();
    }
});

function validateName (name) {
    if(name.value.length > 1){
        name.classList.remove('inputs__error');
        return true;
    }else{
        name.classList.add('inputs__error');
    }
}

function validateEmail (email) {
    if(email.value.length > 1 && checkEmail(email.value)){
        email.classList.remove('inputs__error');
        return true;
    }else{
        email.classList.add('inputs__error');
    }
} 

function checkEmail (str) {
    if (regEmail.test(str)) {
        return true;   
    } else {
        return false
    }
}

function checkPass (pass) {
    if(pass.value.length > 6){
        pass.classList.remove('inputs__error');
        return true;
    }else{
        pass.classList.add('inputs__error');
    }
}

function confirmPass (confirm, pass) {
    if(confirm.value === pass.value && confirm.value.length != ''){
        confirm.classList.remove('inputs__error');
        return true;
    }else{
        confirm.classList.add('inputs__error');
    }
}

